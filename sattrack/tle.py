# tle.py
# Grabs the Two Line Elements from the specified
# URL and then loads it into an array of dicts
#
# Licensed under the GNU General Public License Version 3
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Copyright 2014 John H. Dulaney
#
# Author:
#   John H. Dulaney <jdulaney@fedoraproject.org>
#

import pycurl
from StringIO import StringIO



def curl_file (url):
    """Download file specified by url and return its contents as a string.
    """

    buff = StringIO()
    tle_file = pycurl.Curl()
    tle_file.setopt(tle_file.URL, url)
    tle_file.setopt(tle_file.WRITEDATA, buff)
    tle_file.perform()
    tle_file.close

    body = buff.getvalue()
    buff.close()

    return body


def interpret_tles (tle_list):
    """Return list of dicts containing TLE data
    """
    # ToDo:  Possibly impliment checksumming at some point?
    tles = tle_list.splitlines()

    dict_list = []

    for i in range(0, len(tles), 3):
        tle_dict = dict([('name', tles[i].strip()), \
                ('Satellite Number', tles[i + 1][2:7]), \
                ('Classification', tles[i + 1][7:8]), \
                ('International Designator year', tles[i + 1][9:11]), \
                ('International Designator number', tles[i + 1][11:14]), \
                ('International Designator piece', tles[i + 1][14:16]), \
                ('Epoch Year', tles[i + 1][18:20]), \
                ('Epoch', tles[i + 1][20:33]), \
                ('First Time Derivative of the Mean Motion', tles[i + 1][33:43]), \
                ('Second Time Derivative of Mean Motion', tles[i + 1][44:52]), \
                ('BSTAR drag term', tles[i + 1][53:61]), \
                ('Ephemeris type', tles[i + 1][62:63]), \
                ('Element number', tles[i + 1][64:68]), \
                ('Checksum 1', tles[i + 1][68:]), \
                ('Inclination', tles[i + 2][8:16]), \
                ('Right Ascension of the Ascending Node', tles[i + 2][17:25]), \
                ('Eccentricity', tles[i + 2][26:33]), \
                ('Argument of Perigee', tles[i + 2][34:42]), \
                ('Mean Anomaly', tles[i + 2][43:51]), \
                ('Mean Motion', tles[i + 2][52:63]), \
                ('Revolution number at epoch', tles[i + 2][63:68]), \
                ('Checksum 2', tles[i + 2][68:]) \
                ])

        dict_list.append(tle_dict)

    return dict_list

